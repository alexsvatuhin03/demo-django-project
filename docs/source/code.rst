Документация для программиста
=============================

***********
Модели в БД
***********
.. automodule:: first.models
    :members:


******************
Используемые формы
******************
.. automodule:: first.forms
    :members:

************
View-функции
************
.. automodule:: first.views
    :members:

